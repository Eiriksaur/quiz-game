const CategoryLink = 'https://opentdb.com/api_category.php'

export const getQuestionsFromAPI = (QuestionLink) =>{
    return fetch(QuestionLink).then(respons => respons.json())
    .then(respons => respons.results);
}

export const getCategoriesFromAPI = () =>{
    return fetch(CategoryLink).then(respons => respons.json())
    .then(respons => respons.trivia_categories);
}

